### Installation

You must have npm installed gloablly before running the following command :-

```sh
$ npm install --global gulp
$ npm install
```
### First Run

```sh
$ gulp dev
```

Now open your broswer and go to 'localhost:8888/'


### Commands

Build:

```sh
$ npm start  // start a dev server
$ npm build  // build a production version
$ npm test   // run Jest cli
```
