var Reflux = require('reflux');

var PostActions = require('../actions/PostActions');
var BoardActions = require('../actions/BoardActions');
var CommentActions = require('../actions/CommentActions');

var _ = require('lodash');
var $ = require('jquery');

var API_URL = 'http://api.boardr.io/'

var sweetAlert = require('sweetalert');

module.exports = Reflux.createStore({
  listenables: [PostActions, BoardActions, CommentActions],
  mixins: [Reflux.ListenerMixin],

  init: function(){
    this.listenTo(PostActions.getPosts, this.fetchPosts);
    this.listenTo(PostActions.rateUp, this.ratePostUp);
    this.listenTo(PostActions.rateDown, this.ratePostDown);
    this.listenTo(PostActions.getPostCount, this.fetchPostCount);
    this.listenTo(PostActions.getPost, this.fetchPost);
    this.listenTo(PostActions.createPost, this.doCreatePost);
    this.listenTo(PostActions.deletePost, this.doDeletePost);
  },

  doDeletePost: function(postid){
    console.table(postid);
    $.ajax({
      type: "POST",
      url: API_URL + 'delete',
      data: {
        'type': 'post',
        'id': postid,
        'token': localStorage.getItem('boardrtoken')
      }
    }).done(function(data) {
      if(data.error){
        sweetAlert({
          title: "Something went wrong!",
          text: data.message,
          type: "error",
          showCancelButton: false,
          closeOnConfirm: true,
          showConfirmButton: false,
          timer: 2000,
        });
      }else{
        PostActions.confirmDeletion();
      }
    });
  },

  doCreatePost: function(options){
    $.ajax({
      type: "POST",
      url: API_URL + 'create',
      data: {
        'type': 'post',
        'title': options.title,
        'link': options.link || null,
        'description': options.description,
        'parent_board': options.parent_board,
        'token': localStorage.getItem('boardrtoken')
      }
    }).done(function(data) {
      if(data.error){
        PostActions.sendError(data.message);
      }else{
        sweetAlert({
          title: "Post Created!",
          text: "You will now be redirected to your post",
          type: "success",
          showCancelButton: false,
          closeOnConfirm: true,
          showConfirmButton: false,
          timer: 2000,
        });
        window.location = `/#/board/${options.parent_board}`;
      }
    });
  },

  ratePostUp: function(options){
    $.ajax({
      type: "POST",
      url: API_URL + 'rate',
      data: {
        'token': localStorage.getItem('boardrtoken'),
        'type': 'post',
        'id': options.id,
        'direction': 'up',
      }
    }).done(function(data) {
      if(data.error){
        sweetAlert({
          title: "Something went wrong!",
          text: data.message,
          type: "error",
          showCancelButton: false,
          closeOnConfirm: true,
          showConfirmButton: false,
          timer: 2000,
        });
      }else{
        PostActions.getPosts({parent_board: options.parent_board, limit: 5, page: options.page});
      }
    });
  },

  ratePostDown: function(options){
    $.ajax({
      type: "POST",
      url: API_URL + 'rate',
      data: {
        'token': localStorage.getItem('boardrtoken'),
        'type': 'post',
        'id': options.id,
        'direction': 'down',
      }
    }).done(function(data) {
      if(data.error){
        sweetAlert({
          title: "Something went wrong!",
          text: data.message,
          type: "error",
          showCancelButton: false,
          closeOnConfirm: true,
          showConfirmButton: false,
          timer: 2000,
        });
      }else{
        PostActions.getPosts({parent_board: options.parent_board, limit: 5, page: options.page});
      }
    });
  },


  fetchPostCount: function(options){
    $.ajax({
      type: "POST",
      url: API_URL + 'get',
      data: {
        'type': 'postscount',
        'parent_board': options.parent_board,
      }
    }).done(function(data) {
      PostActions.sendPostCount(data.count);
    });
  },

  fetchPost: function(options){
    $.ajax({
      type: "POST",
      url:  API_URL + 'get',
      data: {
        'type': 'posts',
        'parent_board': options.parent_board,
        'id': options.id,
      }
    }).done(function(data) {
      PostActions.sendPost(data);
    });
  },

  fetchPosts: function(options){
    var toSkip = 0;
    var pageSize = 5;

    if(options.page){
      toSkip = pageSize * (options.page - 1);
    }
    $.ajax({
      type: "POST",
      url:  API_URL + 'get',
      data: {
        'type': 'posts',
        'parent_board': options.parent_board,
        'sort': 'highest_rated',
        'limit': options.limit || 10,
        'skip': toSkip,
      }
    }).done(function(data) {
      PostActions.sendPostsData(data);
    });
  }
});
