var Reflux = require('reflux');

var PostActions = require('../actions/PostActions');
var BoardActions = require('../actions/BoardActions');
var CommentActions = require('../actions/CommentActions');

var _ = require('lodash');
var $ = require('jquery');

var API_URL = 'http://api.boardr.io/'

var sweetAlert = require('sweetalert');

module.exports = Reflux.createStore({
  listenables: [PostActions, BoardActions, CommentActions],
  mixins: [Reflux.ListenerMixin],

  init: function(){
    this.listenTo(BoardActions.getBoards, this.fetchBoards);
    this.listenTo(BoardActions.createBoard, this.doCreateBoard);
    this.listenTo(BoardActions.getOwnedBoards, this.fetchOwnedBoards);
    this.listenTo(BoardActions.delete, this.deleteBoard);
    this.listenTo(BoardActions.togglePrivacy, this.pleaseTogglePrivacy);
  },

  pleaseTogglePrivacy: function(boardName){
    $.ajax({
      type: "POST",
      url: API_URL + 'edit',
      data: {
        'type': 'board',
        'name': boardName,
        'togglePrivacy': true,
        'token': localStorage.getItem('boardrtoken')
      }
    }).done(function(data) {
      if(data.error){
        sweetAlert({
          title: "Something went wrong!",
          text: data.message,
          type: "error",
          showCancelButton: false,
          closeOnConfirm: true,
          showConfirmButton: false,
          timer: 2000,
        });
      }else{
        BoardActions.getOwnedBoards();
      }
    });
  },
  
  deleteBoard: function(options){
    $.ajax({
      type: "POST",
      url: API_URL + 'delete',
      data: {
        'type': 'board',
        'name': options.boardName,
        'token': localStorage.getItem('boardrtoken')
      }
    }).done(function(data) {
      if(data.error){
        sweetAlert({
          title: "Something went wrong!",
          text: data.message,
          type: "error",
          showCancelButton: false,
          closeOnConfirm: true,
          showConfirmButton: false,
          timer: 2000,
        });
      }else{
        BoardActions.confirmDeletion({boardName: options.boardName});
      }
    });
  },

  doCreateBoard: function(options){
    $.ajax({
      type: "POST",
      url: API_URL + 'create',
      data: {
        'type': 'board',
        'name': options.boardName,
        'private': options.private || false,
        'token': localStorage.getItem('boardrtoken')
      }
    }).done(function(data) {
      if(data.error){
        BoardActions.sendError(data.message);
      }else{
        sweetAlert({
          title: "Board Created!",
          text: "You will now be redirected to your new board",
          type: "success",
          showCancelButton: false,
          closeOnConfirm: true,
          showConfirmButton: false,
          timer: 2000,
        });
        window.location = `/#/board/${encodeURLComponent(options.boardName)}`;
      }
    });
  },

  fetchBoards: function(options){
    $.ajax({
      type: "POST",
      url:  API_URL + 'get',
      data: {
        'type': 'boards',
        'limit': 111111111111111,
      }
    }).done(function(data) {
      BoardActions.sendBoardsData(data);
    });
  },

  fetchOwnedBoards: function(options){
    $.ajax({
      type: "POST",
      url:  API_URL + 'get',
      data: {
        'type': 'boards',
        'limit': 111111111111111,
        'token': localStorage.getItem('boardrtoken'),
        'byOwner': true,
      }
    }).done(function(data) {
      BoardActions.sendOwnedBoards(data);
    });
  }
});
