var Reflux = require('reflux');

var PostActions = require('../actions/PostActions');
var BoardActions = require('../actions/BoardActions');
var CommentActions = require('../actions/CommentActions');

var _ = require('lodash');
var $ = require('jquery');

var API_URL = 'http://api.boardr.io/'

var sweetAlert = require('sweetalert');

module.exports = Reflux.createStore({
  listenables: [PostActions, BoardActions, CommentActions],
  mixins: [Reflux.ListenerMixin],

  init: function(){
    this.listenTo(CommentActions.comment, this.doComment);
    this.listenTo(CommentActions.getCommentsByPost, this.fetchComments);
    this.listenTo(CommentActions.getSubComments, this.fetchSubComments);
    this.listenTo(CommentActions.rateUp, this.rateCommentUp);
    this.listenTo(CommentActions.rateDown, this.rateCommentDown);
  },

  doComment: function(options){
    $.ajax({
      type: "POST",
      url: API_URL + 'create',
      data: {
        'type': 'comment',
        'comment': options.comment,
        'parent_board': options.parent_board,
        'parent_post': options.parent_post,
        'parent_comment': options.parent_comment || null,
        'token': localStorage.getItem('boardrtoken')
      }
    }).done(function(data) {
      if(data.error){
        sweetAlert({
          title: "Something went wrong!",
          text: data.message,
          type: "error",
          showCancelButton: false,
          closeOnConfirm: true,
          showConfirmButton: false,
          timer: 2000,
        });
      }else{
        if(options.parent_comment){
          CommentActions.getSubComments({parent_board: options.BoardName, parent_post: options.parent_post, parent_comment: options.parent_comment, limit:15 });
        }else{
          CommentActions.getCommentsByPost({parent_board: options.boardName, parent_post: options.parent_post, limit: 15 });
        }
      }
    });
  },

  rateCommentUp: function(options){
    $.ajax({
      type: "POST",
      url: API_URL + 'rate',
      data: {
        'token': localStorage.getItem('boardrtoken'),
        'type': 'comment',
        'id': options.id,
        'direction': 'up',
      }
    }).done(function(data) {
      if(data.error){
        sweetAlert({
          title: "Something went wrong!",
          text: data.message,
          type: "error",
          showCancelButton: false,
          closeOnConfirm: true,
          showConfirmButton: false,
          timer: 2000,
        });
      }else{
        console.table(data);
        CommentActions.getCommentsByPost({parent_board: options.parentBoard, parent_post: options.parentPost, limit: 15 });
      }
    });
  },

  rateCommentDown: function(options){
    $.ajax({
      type: "POST",
      url: API_URL + 'rate',
      data: {
        'token': localStorage.getItem('boardrtoken'),
        'type': 'comment',
        'id': options.id,
        'direction': 'down',
      }
    }).done(function(data) {
      if(data.error){
        sweetAlert({
          title: "Something went wrong!",
          text: data.message,
          type: "error",
          showCancelButton: false,
          closeOnConfirm: true,
          showConfirmButton: false,
          timer: 2000,
        });
      }else{
        console.table(data);
        CommentActions.getCommentsByPost({parent_board: options.parentBoard, parent_post: options.parentPost, limit: 15 });
      }
    });
  },

  fetchComments: function(options){
    $.ajax({
      type: "POST",
      url:  API_URL + 'get',
      data: {
        'type': 'comments',
        'parent_board': options.parent_board,
        'parent_post': options.parent_post,
        'limit': options.limit || 5
      }
    }).done(function(data) {
      CommentActions.sendComments(data);
    });
  },

  fetchSubComments: function(options){
    $.ajax({
      type: "POST",
      url:  API_URL + 'get',
      data: {
        'type': 'comments',
        'parent_board': options.parent_board,
        'parent_post': options.parent_post,
        'parent_comment': options.parent_comment,
      }
    }).done(function(data) {
      console.table(data);
      CommentActions.sendSubComments(data);
    });
  }
});
