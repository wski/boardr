var version = '0.0.1';

var React = require('react/addons');
var Reflux = require('reflux');

var Router = require('react-router');
var { Route, RouteHandler, Link } = Router;

var PostStore = require('../stores/PostStore');
var CommentStore = require('../stores/CommentStore');
var BoardStore = require('../stores/BoardStore');
var Nav = require('./modules/navbar/navbar.jsx');

module.exports = React.createClass({
  mixins: [ Router.State, Reflux.connect(CommentStore), Reflux.connect(PostStore), Reflux.connect(BoardStore)],

  componentDidMount: function(){
    console.log('%cBoardr Alpha', 'background: #fff; padding: .2em; font-weight: 600; border: 4px solid #333; font-size:20pt; color: #333; display: block;');
    console.log(`%cv${version}`, 'background: transparent; font-weight: 600; font-size:18pt; color: #333; display: block;');
    console.log('have issues? http://github.com/wski/boardr');
    console.log('Type help() for help\n\n');
  },

  render: function(){
    var router = this.context;
    var boardName = router.getCurrentParams().boardName || "Boardr";
    return(
      <div className="paddme">
        <header>
          <h1><Link to="/boards">{boardName}</Link></h1>
        </header>
        <Nav />
        <RouteHandler />

        <footer className="mainFooter">

          <div className="footertext">
            <p>Boardr is a product of Bitmill.io, and is <a href="http://madewithloveinbaltimore.org">Made with &hearts; in Baltimore</a></p>
          </div>
        </footer>
      </div>
    );
  }
});
