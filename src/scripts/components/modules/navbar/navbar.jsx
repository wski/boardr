var React = require('react');

var Router = require('react-router');
var { Route, RouteHandler, Link } = Router;

module.exports = React.createClass({
  mixins: [ Router.State ],
  contextTypes: {
    router: React.PropTypes.func
  },
  render: function(){
    var router = this.context;
    var boardName = router.getCurrentParams().boardName;

    return(
      <nav className="primaryNav">
        <span className="title">Jump To:</span>
        <Link to="boards">boards</Link><span>,</span>
        <Link to="createBoard">create a board</Link><span>,</span>
        <Link to="manageBoards">manage boards</Link><span>,</span>
        <a href="http://api.boardr.io/auth/twitter">login with twitter</a><span>,</span>
        {
          document.URL.split('#')[1].split('/')[1] === 'board' &&
            <Link to="createPost" params={{boardName: boardName}}>create a post</Link>
        }
      </nav>
    );
  }
});
