var React = require('react');
var Reflux = require('reflux');

var Router = require('react-router');
var { Route, RouteHandler, Link } = Router;

var CommentActions = require('../../../actions/CommentActions.jsx');

module.exports = React.createClass({
  mixins: [ Router.State, Reflux.ListenerMixin ],
  contextTypes: {
    router: React.PropTypes.func
  },
  render: function(){
    return(
      <div className="commentmaker">
        <textarea id="comment" placeholder="say something awesome"></textarea><br />
        <button onClick={()=>{this.comment()}} className="btn-default">Comment</button>
      </div>
    );
  },
  comment: function(){
    CommentActions.comment({
      parent_board: this.props.parentboard,
      parent_post: this.props.postid,
      comment: document.getElementById('comment').value,
    });

    document.getElementById('comment').value = '';
  },
});
