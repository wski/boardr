var React = require('react');
var Reflux = require('reflux');

var Router = require('react-router');
var { Route, RouteHandler, Link } = Router;

var CommentActions = require('../../../actions/CommentActions.jsx');

module.exports = React.createClass({
  mixins: [ Router.State, Reflux.ListenerMixin ],
  contextTypes: {
    router: React.PropTypes.func
  },

  comment: function(options){
    CommentActions.comment({
      parent_comment: options.parent_comment,
      comment: options.comment,
      parent_board: this.props.parentBoard,
      parent_post: this.props.parentPost,
    });
  },

  render: function(){
    return(
      <div className="commentmaker">
        <textarea id="comment" ref="comment" placeholder="say something awesome"></textarea><br />
        <button onClick={()=>{this.comment({
            comment: this.refs.comment.getDOMNode().value,
            parent_comment: this.props.parentComment,
            parent_post: this.props.parentPost,
          })}} className="btn-default">Comment</button>
      </div>
    );
  }
});
