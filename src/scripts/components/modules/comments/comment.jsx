var React = require('react');
var Reflux = require('reflux');

var Router = require('react-router');
var { Route, RouteHandler, Link } = Router;

var Sub = require('./sub-comment.jsx');
var Reply = require('./replyBox.jsx');

var CommentActions = require('../../../actions/CommentActions.jsx');

module.exports = React.createClass({
  mixins: [ Router.State, Reflux.ListenerMixin ],
  contextTypes: {
    router: React.PropTypes.func
  },

  getInitialState: function(){
    return{
      subs: null,
      replying: false,
    };
  },

  componentDidMount: function(){
    var router = this.context;
    var boardName = router.getCurrentParams().boardName;
    var page = router.getCurrentParams().page || null;
    var postid = router.getCurrentParams().postid || null;

    CommentActions.getSubComments({
      parent_board: boardName,
      parent_post: decodeURIComponent(postid),
      parent_comment: this.props.data._id
    });

    this.listenTo(CommentActions.sendSubComments, (subs)=>{
      if(subs[0].parent_comment === this.props.data._id){
        this.setState({
          subs: subs
        });
      }
    });
  },

  render: function(){
    var upclass = 'rate_up';
    var downclass = 'rate_down';
    var router = this.context;

    if(this.props.data.ups.length || this.props.data.downs.length){
      upclass = (this.props.data.ups.indexOf(localStorage.getItem('boarderuserid')) > -1) ? 'rate_up active' : 'rate_up';
      downclass = (this.props.data.downs.indexOf(localStorage.getItem('boarderuserid')) > -1) ? 'rate_down active' : 'rate_down';
    }

    var Subs = (this.state.subs) ? this.state.subs.map((sub)=>{
      if (sub.parent_comment === this.props.data._id){
        return(
          <Sub data={sub} />
        );
      }
    }) : '';
    return(
      <div className="comment-block">
        <div className="comment">
          <div className="rate">

            <button className={upclass} onClick={()=>{this.rateUp(this.props.data._id)}}>
              <i className="fa fa-chevron-up"></i>
            </button>

            <h3 className="score">{this.props.data.score}</h3>

            <button className={downclass} onClick={()=>{this.rateDown(this.props.data._id)}}>
              <i className="fa fa-chevron-down"></i>
            </button>

          </div>

          <div className="post_content">
            <p className="post_description">{this.props.data.comment || ''}</p>
          </div>

          <p className="post_info">posted by {this.props.data.owner}, on {this.props.data.timestamp}</p>

          <div className="replyButton" onClick={()=>{
              this.setState({
                replying: !this.state.replying
              });
            }}>
            <i className="fa fa-reply"></i> Reply
          </div>
        </div>
        <div className="replyBoxHolder">
          { this.state.replying &&
            <Reply parentComment={this.props.data._id}
              parentBoard={decodeURIComponent(router.getCurrentParams().boardName)}
              parentPost={decodeURIComponent(router.getCurrentParams().postid)} />
          }
        </div>
        <div className="sub-comments">
          {Subs}
        </div>
      </div>
    );
  },

  rateUp: function(){
    var router = this.context;
    var boardName = router.getCurrentParams().boardName;
    var parentPost = router.getCurrentParams().postid;

    CommentActions.rateUp({
      parentBoard: boardName,
      parentPost: decodeURIComponent(parentPost),
      id: this.props.data._id
    });
  },

  rateDown: function(){
    var router = this.context;
    var boardName = router.getCurrentParams().boardName;
    var parentPost = router.getCurrentParams().postid;

    CommentActions.rateDown({
      parentBoard: boardName,
      parentPost: decodeURIComponent(parentPost),
      id: this.props.data._id
    });
  },
});
