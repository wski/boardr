var React = require('react');

var Router = require('react-router');
var { Route, RouteHandler, Link } = Router;

module.exports = React.createClass({
  mixins: [ Router.State ],
  contextTypes: {
    router: React.PropTypes.func
  },
  render: function(){
    return(
      <div className="sub-comment-block">
        <div className="comment">
          <div className="rate">

            <button className="rate_up">
              <i className="fa fa-chevron-up"></i>
            </button>

            <h3 className="score">{this.props.data.score || 0}</h3>

            <button className="rate_down">
              <i className="fa fa-chevron-down"></i>
            </button>

          </div>

          <div className="post_content">
            <p className="post_description">{this.props.data.comment || ''}</p>
          </div>

          <p className="post_info">posted by {this.props.data.owner}, on {this.props.data.timestamp}</p>
        </div>
      </div>
    );
  }
});
