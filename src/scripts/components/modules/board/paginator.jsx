var React = require('react');
var PostActions = require('../../../actions/PostActions.jsx');

var Router = require('react-router');
var { Route, RouteHandler, Link } = Router;

module.exports = React.createClass({
  mixins: [ Router.State ],
  contextTypes: {
    router: React.PropTypes.func
  },
  render: function(){
    var pending_page = (parseInt(this.props.page) != parseInt(this.props.pagemax)) ? parseInt(this.props.page)+1 : this.props.page;
    return(
      <div className="pagination-container">

        <ul className="pagination">

          <li className="pagination-item--wide first">
            <Link className="pagination-link--wide first" to={`/board/${this.props.board}/${(this.props.page > 1) ? this.props.page -1 : 1}`} onClick={()=>{
                  PostActions.getPosts({parent_board: this.props.board, limit: 5, page: parseInt(this.props.page)-1});
                }}><i className="fa fa-chevron-left"></i> &nbsp; Previous</Link>
          </li>

          <li className="pagination-item">
            <span className="pagination-number">{this.props.page}/{this.props.pagemax}</span>
          </li>

          <li className="pagination-item--wide last">
            <Link className="pagination-link--wide last" to={`/board/${this.props.board}/${pending_page}`} onClick={()=>{
                  PostActions.getPosts({parent_board: this.props.board, limit: 5, page: pending_page});
                }}>Next &nbsp; <i className="fa fa-chevron-right"></i></Link>
          </li>

        </ul>

      </div>
    );
  }
});
