var React = require('react');
var Reflux = require('reflux');
var PostActions = require('../../../actions/PostActions.jsx');

var Router = require('react-router');
var { Route, RouteHandler, Link } = Router;

var sweetAlert = require('sweetalert');

module.exports = React.createClass({
  mixins: [ Router.State, Reflux.ListenerMixin ],
  contextTypes: {
    router: React.PropTypes.func
  },

  getInitialState: function(){
    return {
      menuOpened: false,
    };
  },

  rateUp: function(id){
    var router = this.context;
    var page = router.getCurrentParams().page;

    PostActions.rateUp({
      id: id,
      parent_board: this.props.parentBoard,
      page: page || 1
    });
  },
  rateDown: function(id){
    var router = this.context;
    var page = router.getCurrentParams().page;

    PostActions.rateDown({
      id: id,
      parent_board: this.props.parentBoard,
      page: page || 1
    });
  },
  deletePost: function(postid){
    sweetAlert({
      title: `Delete Post?`,
      text: "You will not be able to undo this! All child comments will be nuked. Gone forever. Bye Byezies.",
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: true,
      showConfirmButton: true,
    }, function(){
        PostActions.deletePost(postid);
    });
  },
  componentDidMount: function(){
    this.listenTo(PostActions.confirmDeletion, ()=>{
      setTimeout(function(){
        sweetAlert({
          title: "Deleted Board!",
          text: "You board has been sucessfully deleted! (I hope you meant it!)",
          type: "success",
          showCancelButton: false,
          closeOnConfirm: true,
          showConfirmButton: false,
          timer: 2000,
        });
        BoardActions.getOwnedBoards();
      }, 200);
    });
  },
  render: function(){
    var upclass = 'rate_up';
    var downclass = 'rate_down';

    if(this.props.data.ups.length || this.props.data.downs.length){
      upclass = (this.props.data.ups.indexOf(localStorage.getItem('boarderuserid')) > -1) ? 'rate_up active' : 'rate_up';
      downclass = (this.props.data.downs.indexOf(localStorage.getItem('boarderuserid')) > -1) ? 'rate_down active' : 'rate_down';
    }

    var router = this.context;
    var boardName = router.getCurrentParams().boardName;

    var title = (this.props.data.link) ? <a target="_blank" href={this.props.data.link}><h2 className="post_title">{this.props.data.title} <i className="fa fa-link"></i></h2></a> :
      <h2 className="post_title">{this.props.data.title}</h2>;
    return(
      <div className="post">
        { (this.props.data.owner === parseInt(localStorage.getItem('boardruserid'))) &&
          <div className="postMenu">
            <div className="postMenuButton" onClick={()=>{
                this.setState({menuOpened: !this.state.menuOpened});
              }}>
              <i className="fa fa-cog"></i>
            </div>
            { this.state.menuOpened &&
              <div className="postMenuThing">
                  <a onClick={()=>{
                      this.deletePost(this.props.data._id);
                    }}><i className="fa fa-trash-o"></i> Delete Post</a><br />
                  <a><i className="fa fa-pencil"></i> Edit Post</a>
              </div>
            }
          </div>
        }

        <div className="rate">

          <button className={upclass} onClick={()=>{this.rateUp(this.props.data._id)}}>
            <i className="fa fa-chevron-up"></i>
          </button>

          <h3 className="score">{this.props.data.score || 0}</h3>

          <button className={downclass} onClick={()=>{this.rateDown(this.props.data._id)}}>
            <i className="fa fa-chevron-down"></i>
          </button>

        </div>

        <div className="post_content">
          {title}
          <p className="post_description">{this.props.data.description || ''}</p>
        </div>

        <p className="post_info">posted by {this.props.data.owner}, on {this.props.data.timestamp}</p>

        <Link to={`/board/${boardName}/${encodeURIComponent(this.props.data.title)}/comments`}>
          <button className="post_launch">
            <i className="fa fa-chevron-right"></i>
          </button>
        </Link>
      </div>
    );
  }
});
