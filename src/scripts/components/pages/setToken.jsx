var Reflux = require('reflux');
var React = require('react');

var Router = require('react-router');
var { Route, RouteHandler, Link } = Router;

module.exports = React.createClass({
  mixins: [ Router.State, Reflux.ListenerMixin],

  contextTypes: {
    router: React.PropTypes.func
  },

  componentDidMount: function(){
    var router = this.context;
    var token = router.getCurrentParams().token || null;
    var userid = router.getCurrentParams().userid || null;

    if (token){
      localStorage.setItem('boardrtoken', token);
      localStorage.setItem('boardruserid', userid);

      window.location = '/#';
    }
  },

  render: function(){
    return (
      <h3>
        Setting Token
      </h3>
    );
  }
});
