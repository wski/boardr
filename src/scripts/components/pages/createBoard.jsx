var Reflux = require('reflux');

var React = require('react');
var BoardActions = require('../../actions/BoardActions.jsx');

var Router = require('react-router');
var { Route, RouteHandler, Link } = Router;

var Nav = require('../modules/navbar/navbar.jsx');

module.exports = React.createClass({
  mixins: [ Router.State, Reflux.ListenerMixin],

  contextTypes: {
    router: React.PropTypes.func
  },

  getInitialState: function(){
      return { error: null };
  },

  componentDidMount: function(){
    this.listenTo(BoardActions.sendError, (error)=>{
      this.setState({error: error});
    });
  },

  createBoard: function(){
    this.setState({error: null});

    BoardActions.createBoard({
      boardName : document.getElementById('boardTitle').value,
      private   : document.getElementById('privateBoard').checked,
    });

  },

  render: function(){
    return (
      <div>
        <fieldset>
          <h2><span>Create A Board</span></h2>
          <p className="description">Make it special, the world is waiting.</p>
        </fieldset>
        <div className="boardsList">
          <div className="center fifty">
            <label>Board Title:</label><br /><br />
            <input type="text" id="boardTitle" placeholder="board title..."></input><br /><br/>
            <hr className="prettyHR"/>
            <label>Make this board read only?</label>
            <p>People will be able to read all the posts on your board, but not post things of their own.</p>
            <input id="privateBoard" type="checkbox" className="regular-radio"></input> <br /><br />
            <hr className="prettyHR"/>
            <button className="btn-default" onClick={this.createBoard.bind()}>Create Board</button>
            { this.state.error &&
                <div className="error">{this.state.error}</div>
            }
          </div>
        </div>
      </div>
    );
  }
});
