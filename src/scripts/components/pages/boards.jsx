var Reflux = require('reflux');

var React = require('react');
var BoardActions = require('../../actions/BoardActions.jsx');

var Router = require('react-router');
var { Route, RouteHandler, Link } = Router;

var Nav = require('../modules/navbar/navbar.jsx');

module.exports = React.createClass({
  mixins: [ Router.State, Reflux.ListenerMixin],

  contextTypes: {
    router: React.PropTypes.func
  },

  getInitialState: function(){
    return{ boards : null };
  },

  componentDidMount: function(){
    BoardActions.getBoards();

    this.listenTo(BoardActions.sendBoardsData, (data)=>{
      this.setState({boards: data});
    });
  },

  componentDidUpdate: function(){


  },

  render: function(){
    var router = this.context;
    var boardName = router.getCurrentParams().boardName;
    var alphabet = ['a','b','c','d','e','f','g','h'
			,'i','j','k','l','m','n','o','p','q'
			,'r','s','t','u','v','w','x','y','z'];
    var Boards = (this.state.boards) ? (this.state.boards.length > 0) ? this.state.boards.map(function(board){
      if(alphabet.indexOf(board.name.charAt(0).toLowerCase()) > -1){
        var letter = alphabet[alphabet.indexOf(board.name.charAt(0).toLowerCase())].toUpperCase();
        delete alphabet[alphabet.indexOf(board.name.charAt(0).toLowerCase())];
        return(
          <span>
            <p className="boardIndex">{letter}</p>
            <Link className="boardLink" to={'/board/' + board.name}>{board.name} { board.private &&
              <i className="fa fa-eye"></i>
            }</Link>
          </span>
        );
      }else{
        return(<Link className="boardLink" to={'/board/' + board.name}>{board.name} { board.private &&
          <i className="fa fa-eye"></i>
        }</Link>);
      }
    }) : <h3>Weird, no boards here</h3> : <h3>Loading Boards...</h3>;
    return (
      <div>
        <fieldset>
          <h2><span>Boards</span></h2>
          <p className="description">Our Current Boards</p>
        </fieldset>
        <div className="boardsList">
          {Boards}
        </div>
      </div>
    );
  }
});
