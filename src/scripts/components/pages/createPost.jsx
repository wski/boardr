var Reflux = require('reflux');

var React = require('react');
var PostActions = require('../../actions/PostActions.jsx');

var Router = require('react-router');
var { Route, RouteHandler, Link } = Router;

var Nav = require('../modules/navbar/navbar.jsx');

module.exports = React.createClass({
  mixins: [ Router.State, Reflux.ListenerMixin],

  contextTypes: {
    router: React.PropTypes.func
  },

  getInitialState: function(){
      return { error: null, location: null };
  },

  componentDidMount: function(){
    this.listenTo(PostActions.sendError, (error)=>{
      this.setState({error: error});
    });
  },

  createPost: function(){
    this.setState({error: null});

    var requestTimer = true;
    var router = this.context;
    var boardName = router.getCurrentParams().boardName;

    PostActions.createPost({
      title: document.getElementById('postTitle').value,
      description: document.getElementById('postBody').value,
      link: document.getElementById('postLink').value,
      location: this.state.location,
      parent_board: boardName,
    });
  },

  render: function(){
    return (
      <div>
        <fieldset>
          <h2><span>Create A Post</span></h2>
          <p className="description">Make it special, the world is waiting.</p>
        </fieldset>
        <div className="boardsList">
          <div className="center fifty">
            <label>Post Title:</label><br /><br />
            <input type="text" id="postTitle" placeholder="post title..."></input><br /><br/>

            <label>Post Content:</label><br /><br />
            <textarea id="postBody" placeholder="you will not belive what i saw today..."></textarea><br /><br/>

            <label>Related Link (optional):</label><br /><br />
            <input type="text" id="postLink" placeholder="optional related link..."></input><br /><br/>

            <button className="btn-default map-marker" onClick={()=>{
                var setter = (location)=>{
                  this.setState({
                    location: location,
                  });
                };
                if (navigator.geolocation) {
                  var location = navigator.geolocation.getCurrentPosition(setter);
                }
              }}><i className="fa fa-map-marker"></i> Geotag Post</button>

            { this.state.location &&
              <p className="locationText">{ `${this.state.location.coords.longitude},\n ${this.state.location.coords.latitude}`}</p>
            }
            <button className="btn-default" onClick={this.createPost.bind()}>Create Post</button><br /><br />
            { this.state.error &&
                <div className="error">{this.state.error}</div>
            }
          </div>
        </div>
      </div>
    );
  }
});
