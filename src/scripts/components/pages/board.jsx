var Reflux = require('reflux');

var React = require('react');
var PostActions = require('../../actions/PostActions.jsx');

var Router = require('react-router');
var { Route, RouteHandler, Link } = Router;

var Nav = require('../modules/navbar/navbar.jsx');
var Post = require('../modules/board/post.jsx');
var Paginator = require('../modules/board/paginator.jsx');


module.exports = React.createClass({
  mixins: [ Router.State, Reflux.ListenerMixin],

  contextTypes: {
    router: React.PropTypes.func
  },

  getInitialState: function(){
    return{
      posts     : null,
      page      : null,
      postCount : null
    };
  },

  componentDidMount: function(){
    var router = this.context;
    var boardName = router.getCurrentParams().boardName;
    var page = router.getCurrentParams().page || null;

    if (page){
      this.setState({page:page});
    }
    PostActions.getPosts({parent_board: boardName, limit: 5, page: page});
    PostActions.getPostCount({parent_board: boardName});

    this.listenTo(PostActions.sendPostsData, (data)=>{
      this.setState({posts: data});
    });

    this.listenTo(PostActions.sendPostCount, (data)=>{
      this.setState({postCount: data});
    });
  },

  componentDidUpdate: function(){


  },

  render: function(){
    var router = this.context;
    var boardName = router.getCurrentParams().boardName;
    var page = router.getCurrentParams().page || 1;

    var Posts = (this.state.posts) ? this.state.posts.map(function(post){
      return(<Post data={post} parentBoard={boardName} />);
    }) : null;

    return (
      <div>
        <fieldset>
          <h2><span>Posts</span></h2>
          <p className="description">Top rated posts from the {boardName} board.</p>
        </fieldset>

        <div className="posts">
          { this.state.posts &&
              Posts
          }
        </div>
        <Paginator page={page} pagemax={Math.ceil(this.state.postCount / 5)} board={boardName}/>
      </div>
    );
  }
});
