var Reflux = require('reflux');
var React = require('react');

var CommentActions = require('../../actions/CommentActions.jsx');
var PostActions = require('../../actions/PostActions.jsx');

var Router = require('react-router');
var { Route, RouteHandler, Link } = Router;

var Nav = require('../modules/navbar/navbar.jsx');
var Comment = require('../modules/comments/comment.jsx');
var CreateComment = require('../modules/comments/createComment.jsx');
module.exports = React.createClass({
  mixins: [ Router.State, Reflux.ListenerMixin],

  contextTypes: {
    router: React.PropTypes.func
  },

  getInitialState: function(){
    return{ post: null, comments: null };
  },

  componentDidMount: function(){
    var router = this.context;
    var boardName = router.getCurrentParams().boardName;
    var page = router.getCurrentParams().page || null;
    var postid = router.getCurrentParams().postid || null;

    CommentActions.getCommentsByPost({parent_board: boardName, parent_post: decodeURIComponent(postid), limit: 15 });

    this.listenTo(CommentActions.sendComments, (comments)=>{
      this.setState({
        comments: comments
      });
    });

    PostActions.getPost({parent_board: boardName, parent_post: decodeURIComponent(postid) });

    this.listenTo(PostActions.sendPost, (data)=>{
      this.setState({post: data});
    });
  },

  render: function(){
    var router = this.context;
    var boardName = router.getCurrentParams().boardName;
    var postid = router.getCurrentParams().postid;

    var Comments = (this.state.comments) ? (this.state.comments.length > 0) ? this.state.comments.map(function(comment){
      if(comment.parent_comment === 'root_level'){
        return(
          <Comment data={comment} />
        );  
      }
    }) : <h3>Looks like there are no comments, Why not be the first?</h3> : <i className="fa fa-circle-o-notch fa-spin"></i>;

    return (
      <div className="onehundred">
        <fieldset>
          <h2><span>{(this.state.post) ? this.state.post[0].title : ""}</span></h2>
          <p className="postDescription">{(this.state.post) ? this.state.post[0].description : ""}</p>
        </fieldset>
        <fieldset>
          <h2><span>Comments</span></h2>
          <p className="description">Comments on the {boardName} board.</p>
        </fieldset>
        <CreateComment parentboard={boardName} postid={decodeURIComponent(postid)} />
        <div className="commentsList">
          { Comments }
        </div>
      </div>
    );
  }
});
