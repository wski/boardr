var Reflux = require('reflux');

var React = require('react');
var BoardActions = require('../../actions/BoardActions.jsx');

var Router = require('react-router');
var { Route, RouteHandler, Link } = Router;

var Nav = require('../modules/navbar/navbar.jsx');
var sweetAlert = require('sweetalert');

module.exports = React.createClass({
  mixins: [ Router.State, Reflux.ListenerMixin],

  contextTypes: {
    router: React.PropTypes.func
  },

  getInitialState: function(){
    return{ boards : null };
  },

  componentDidMount: function(){
    BoardActions.getOwnedBoards();

    this.listenTo(BoardActions.sendOwnedBoards, (data)=>{
      this.setState({boards: data});
    });

    this.listenTo(BoardActions.confirmDeletion, ()=>{
      setTimeout(function(){
        sweetAlert({
          title: "Deleted Board!",
          text: "You board has been sucessfully deleted! (I hope you meant it!)",
          type: "success",
          showCancelButton: false,
          closeOnConfirm: true,
          showConfirmButton: false,
          timer: 2000,
        });
        BoardActions.getOwnedBoards();

      }, 200);
    });
  },

  togglePrivacy: function(boardname){
    BoardActions.togglePrivacy(boardname);
  },

  deleteBoard: function(boardname){
    sweetAlert({
      title: `Delete ${boardname}?`,
      text: "You will not be able to undo this! All child posts and comments will be nuked. Gone forever. Bye Byezies.",
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: true,
      showConfirmButton: true,
    }, function(){
        BoardActions.delete({boardName: boardname});
    });
  },

  render: function(){
    var router = this.context;

    var Boards = (this.state.boards) ? (this.state.boards.length > 0) ? this.state.boards.map((board)=>{
        return(
          <p className="board_row">
            <span className="board_name">{board.name}</span>
            <span className="private_board">
              <input id="privateBoard" type="checkbox" onClick={()=>{
                    this.togglePrivacy(board.name);
                  }} defaultChecked={board.private} className="regular-radio"></input> <br /><br />
            </span>
            <span className="delete_board">
              <button onClick={()=>{
                    this.deleteBoard(board.name);
                  }} className="btn-default">
                <i className="fa fa-trash-o"></i>
              </button>
            </span>
          </p>
        );
    }) : <h3>Looks like you have no boards</h3> : <h3>Loading Boards...</h3>;
    return (
      <div>
        <fieldset>
          <h2><span>Manage Your Boards</span></h2>
          <p className="description">Your Currently Owned Boards</p>
        </fieldset>
        <div className="boardsList">
          <p className="boardTitles"><span>Name</span><span>Delete</span><span>ReadOnly</span></p>
          {Boards}
        </div>
      </div>
    );
  }
});
