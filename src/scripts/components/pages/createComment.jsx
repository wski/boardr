var Reflux = require('reflux');

var React = require('react');
var PostActions = require('../../actions/PostActions.jsx');

var Router = require('react-router');
var { Route, RouteHandler, Link } = Router;

var Nav = require('../modules/navbar/navbar.jsx');

module.exports = React.createClass({
  mixins: [ Router.State, Reflux.ListenerMixin],

  contextTypes: {
    router: React.PropTypes.func
  },

  getInitialState: function(){
      return { error: null };
  },

  componentDidMount: function(){
    this.listenTo(PostActions.sendError, (error)=>{
      this.setState({error: error});
    });
  },

  createPost: function(){
    this.setState({error: null});

    var requestTimer = true;
    var router = this.context;
    var boardName = router.getCurrentParams().boardName;

    PostActions.createPost({
      title: document.getElementById('postTitle').value,
      description: document.getElementById('postBody').value,
      link: document.getElementById('postLink').value,
      parent_board: boardName,
    });
  },

  render: function(){
    return (
      <div>
        
      </div>
    );
  }
});
