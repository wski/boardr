require('../../../public/main.css');

var React = require('react');

// Assign React to Window so the Chrome React Dev Tools will work.
window.React = React;

var Router = require('react-router');
var {Route, NotFoundRoute} = Router;


// Require route components.
var App = require('./app');

var routes = (
  <Route handler={App}>
    <Route name="post" handler={require('./pages/post')} path="/board/:boardName/:postid/comments" addHandlerKey={true} />

    <Route name="createPost" handler={require('./pages/createPost')} path="/create/post/:boardName" addHandlerKey={true}></Route>
    <Route name="createBoard" handler={require('./pages/createBoard')} path="/create/board" addHandlerKey={true}></Route>

    <Route name="token" handler={require('./pages/setToken')} path="/token/:token/:userid" addHandlerKey={true} />

    <Route name="home" handler={require('./pages/boards')} path="/" addHandlerKey={true} />
    <Route name="boards" handler={require('./pages/boards')} path="/boards" addHandlerKey={true} />
    <Route name="board" handler={require('./pages/board')} path="/board/:boardName" addHandlerKey={true} />
    <Route name="boardPage" handler={require('./pages/board')} path="/board/:boardName/:post" addHandlerKey={true} />

    <Route name="manageBoards" handler={require('./pages/manageBoards')} path="/manage/boards" addhandlerKey={true}></Route>

  </Route>
);

Router.run(routes, function(Handler){
  React.render(<Handler/>, document.getElementById("container"));
});
