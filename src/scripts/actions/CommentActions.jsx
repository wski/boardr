var Reflux = require('reflux');

actions = [
    'getCommentsByPost', 'sendComments', 'getSubComments', 'sendSubComments',
    'rateDown', 'rateUp', 'sendError', 'comment'
];

module.exports = Reflux.createActions(actions);
