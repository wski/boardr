var Reflux = require('reflux');

actions = [
   'getBoards', 'sendBoardsData', 'createBoard', 'sendError', 'getOwnedBoards',
   'sendOwnedBoards', 'delete', 'confirmDeletion', 'togglePrivacy'
];

module.exports = Reflux.createActions(actions);
