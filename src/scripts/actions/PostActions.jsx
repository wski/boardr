var Reflux = require('reflux');

actions = [
    'getPosts', 'sendPostsData', 'rateUp', 'getPostCount', 'sendPostCount',
    'getPost', 'sendPost', 'rateDown','sendError', 'createPost', 'deletePost',
    'confirmDeletion'
];

module.exports = Reflux.createActions(actions);
